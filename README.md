# Kubernetes Nginx example
**1 -** Install docker and k3d.  
**2 -** Create a test cluster:
```bash
k3d create --name nginx --publish 80:80 --volume ~/mnt/data:/mnt/data
```
**3 -** Create the Nginx service.  The pods will be accessible at http://nginx.localhost:
```bash
kubectl apply -f ./bases -R
```
**4 -** Test it worked:
```bash
curl http://nginx.localhost
```

# Notes
* Anything in your `~/mnt/data` directory will be mounted in Nginx container's `/usr/share/nginx/html`.
* The PersistentVolumeClaim will only work on clusters with one node because of the `ReadWriteOnce` access mode.  You could fix this by using a [storage class](https://kubernetes.io/docs/concepts/storage/storage-classes/).
* The alphanumeric order the `.yaml` is processed by the recurssive `-R` is important.  If you add resources, makes sure the `.yaml` that creates them is processed before they're referenced.
* The [NetworkPolicy](https://gitlab.com/patheard/k8s-nginx-example/-/blob/master/bases/core/03-networkpolicy.yaml) rules do not work if using k3d, since the [underlying k3s CNI does not support them](https://github.com/rancher/k3s/issues/445).
